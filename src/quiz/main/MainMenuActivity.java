package quiz.main;

import java.lang.reflect.Field;
import java.util.Random;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenuActivity extends Activity {

	//Haven't found a non-globalizing solution to passing values into OnClickListeners
	String correct = "",label = "";		
	
	@Override
    public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        View.OnClickListener attemptedAnswer;	
        
        Button[] b = {(Button) findViewById(R.id.button1),(Button) findViewById(R.id.button2),(Button) findViewById(R.id.button3),(Button) findViewById(R.id.button4)};

        Random rand = new Random();
        final int amountOfQuestions = ((R.drawable.class.getFields().length - 1)/4) + R.raw.class.getFields().length;
        /*
         * This isn't a great formula yet because any non-question images will add to the count.
         * This is why there's a minus one in there.
         * Also hasn't dealt with !4 images
         * deals with audio now though.
         */
        
        int question = rand.nextInt(amountOfQuestions + 1);
        setQuestions(question);
        //TODO: non-repetitive questions
       
        attemptedAnswer = new View.OnClickListener() {		//An answer has been picked.
            public void onClick(View v) {
            	if(((Button)v).getText().equals(correct)){ 
            		Toast.makeText(v.getContext(), "Correct :)", Toast.LENGTH_SHORT).show();
            	}
            	else{
            		Toast.makeText(v.getContext(), "Incorrect, It was actually " + correct, Toast.LENGTH_SHORT).show();
            	}
                //Return new question to the user
            	Random rand = new Random();
                int question = rand.nextInt(amountOfQuestions + 1);
                setQuestions(question);
            }
        };
        b[0].setOnClickListener(attemptedAnswer);b[1].setOnClickListener(attemptedAnswer);	//attach them to the buttons
        b[2].setOnClickListener(attemptedAnswer);b[3].setOnClickListener(attemptedAnswer);
    }

	
    public void setQuestions(int question){
    	
    	//Prefixed Resources
        label = "q" + question;
        
        int resAmount = 0;
        int questionType = 0;
        /* 0 = Image Question
         * 1 = Audio Question
         */
        
        //making sure we have the right question resources...
        
        Field[] fields = R.drawable.class.getFields();
        Field[] rawFields = R.raw.class.getFields();
        
     	//Declare audio question if it exists, so that we only load in audio resources and don't both with images
        for (Field field : rawFields) {
        	if (field.getName().startsWith(label)) {
        		questionType = 1;						
            }
        }        
        if(questionType == 0){
        	//count the amount of images for this particular question
	        for (Field field : fields) {
	        	if (field.getName().startsWith(label)) {
	                	resAmount++;
	            }
	        }
        }
        
        //retrieve question info from XML, create answer buttons
        Resources res = getResources();
        String[] qres = res.getStringArray(getResources().getIdentifier(label, "array", this.getApplicationInfo().packageName));	//finds by name instead of by id.
        TextView tv = (TextView) findViewById(R.id.question);     //Question Label      
        tv.setText(qres[0]);									  // & Answer Buttons
        Button[] b = {(Button) findViewById(R.id.button1),(Button) findViewById(R.id.button2),(Button) findViewById(R.id.button3),(Button) findViewById(R.id.button4)};
        b[0].setText(qres[1]);b[1].setText(qres[2]);b[2].setText(qres[3]);b[3].setText(qres[4]);
	    correct = qres[5];										  //Correct Value (as specified by XML)

        //Distinguishing Question Types (for layout, etc)
	    //4-Image Question
	    
	    GridView gridview = (GridView) findViewById(R.id.gridview);			//2x2 grid of images
	    ImageView replay = (ImageView) findViewById(R.id.replay);			//1 image (currently the replay button)
        OnClickListener replayAction = new View.OnClickListener() {
        	//if the repeat ImageView is clicked, replay the sound
            public void onClick(View v) {
            	playAlertTone(v.getContext(),label);
            }
        };
        replay.setOnClickListener(replayAction);
	    if(resAmount == 4){
	    	gridview.setVisibility(View.VISIBLE);
        	replay.setVisibility(View.GONE);		//hide the other view.
	    	String[] labels = {label+"_1",label+"_2",label+"_3",label+"_4"};		//format like q1_3.png
	        gridview.setAdapter(new MediaAdapter(this,new Integer[] {
	        		getResources().getIdentifier(labels[0], "drawable", this.getApplicationInfo().packageName),
	        		getResources().getIdentifier(labels[1], "drawable", this.getApplicationInfo().packageName),
	        		getResources().getIdentifier(labels[2], "drawable", this.getApplicationInfo().packageName),
	        		getResources().getIdentifier(labels[3], "drawable", this.getApplicationInfo().packageName)
	        		},0));
        }
        if(questionType == 1){
        	replay.setVisibility(View.VISIBLE);
            gridview.setVisibility(View.GONE);
            playAlertTone(this.getApplicationContext(),label);
        }
	     
    }
    //play the audio for question <label>
    public  void playAlertTone(final Context context,String label){
    	MediaPlayer mp = MediaPlayer.create(getApplicationContext(), getResources().getIdentifier(label, "raw", this.getApplicationInfo().packageName)); mp.start();
    }
}